<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class AdminSettingsTest extends TestCase
{
    use DatabaseMigrations;

    public function testDashboard()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
            ->get('/admin');
        $response->assertStatus(200);
    }

    public function testSettingsManagementIndex()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
            ->get('/admin/settings');
        $response->assertStatus(200);
    }
}
