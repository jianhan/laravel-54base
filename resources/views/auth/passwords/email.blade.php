@extends('layouts.robust.auth')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <form class="form-horizontal form-simple" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
        <fieldset class="form-group position-relative has-icon-left mb-0">
            <input name="email" value="{{ old('email') }}"
                   type="text"
                   class="form-control form-control-lg input-lg"
                   id="email"
                   placeholder="Your Email" required>
            <div class="form-control-position">
                <i class="icon-head"></i>
            </div>
        </fieldset>
        <fieldset class="form-group row">
            <div class="col-md-6 col-xs-12 text-xs-center text-md-left">
                <fieldset>

                </fieldset>
            </div>
            <div class="col-md-6 col-xs-12 text-xs-center text-md-right"><a
                        href="{{route('login')}}" class="card-link">Login</a>
            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary btn-lg btn-block"><i
                    class="icon-check"></i> Send Password Reset Link
        </button>
    </form>
@endsection

@section('page_title')
    Reset Password
@stop
