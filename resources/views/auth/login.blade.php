@extends('layouts.robust.auth')

@section('content')
    <form class="form-horizontal form-simple" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <fieldset class="form-group position-relative has-icon-left mb-0">
            <input name="email" value="{{ old('email') }}"
                   type="text"
                   class="form-control form-control-lg input-lg"
                   id="email"
                   placeholder="Your Email" required>
            <div class="form-control-position">
                <i class="icon-head"></i>
            </div>
        </fieldset>
        <fieldset class="form-group position-relative has-icon-left">
            <input type="password" class="form-control form-control-lg input-lg"
                   name="password" placeholder="Enter Password" required>
            <div class="form-control-position">
                <i class="icon-key3"></i>
            </div>
        </fieldset>
        <fieldset class="form-group row">
            <div class="col-md-6 col-xs-12 text-xs-center text-md-left">
                <fieldset>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label for="remember-me"> Remember Me</label>
                </fieldset>
            </div>
            <div class="col-md-6 col-xs-12 text-xs-center text-md-right"><a
                        href="{{route('password.request')}}" class="card-link">Forgot Password?</a>
            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary btn-lg btn-block"><i
                    class="icon-unlock2"></i> Login
        </button>
        @captcha()
        <a href="{{url('/login/github')}}">github</a>
        <a href="{{url('/login/facebook')}}">facebook</a>
    </form>
@endsection

@section('page_title')
    Authentication
@stop
