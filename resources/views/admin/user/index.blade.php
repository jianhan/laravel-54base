@extends("layouts.robust.admin")

@section("content_header")
    @include("layouts.robust.partials.content_header", ["title" => "Users"])
@stop

@section("content")
    <div class="row">
        <div class="col-xs-12">
            @component('admin.partials.card', ['cardTitle' => 'Manage Users'])
                <div class="row">
                    <div class="col-xs-12 col-md-2">
                        @include("admin.partials.items_per_page", ["itemsPerPage" => $itemsPerPage, "target" => "user"])
                    </div>
                    <div class="col-xs-12 col-md-5">
                        {{Form::open(['route' => 'users.index','method' => 'GET', 'class' => 'form', 'style' => "margin-top: 1rem; margin-bottom: 1rem;"])}}
                        <div class="row">
                            <div class="col-md-10">
                                <div class="input-group">
                                    {{Form::text('query',
                                                trim(Request::query('query')),
                                                ['name' => 'query', 'class' => 'form-control', 'placeholder' => 'Search...'])}}
                                    {{Form::hidden('itemsPerPage', $itemsPerPage)}}
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" type="submit">
                                            <i class="icon-search7"></i>
                                        </button>
                                        <a class="btn btn-secondary"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="Reset Search"
                                           data-original-title="Reset Search"
                                           href="{{route('users.index')}}">
                                            <i class="icon-repeat2"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                    <div class="col-xs-12 col-md-5">
                        <div class="pull-right">{{$users->appends(\Request::except('page'))->render()}}</div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-condensed table-striped">
                        <thead>
                        <tr>
                            <th><i class="icon-head"></i> @sortablelink('name', 'Name')</th>
                            <th><i class="icon-mail6"></i> @sortablelink('email', 'Email')</th>
                            <th><i class="icon-users"></i> Roles</th>
                            <th><i class="icon-check2"></i> @sortablelink('active', 'Active')</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    @foreach($user->roles as $role)
                                        <span class="tag tag-default"
                                              data-toggle="tooltip"
                                              data-placement="top"
                                              title="{{$role->description}}"
                                              data-original-title="{{$role->name}}">{{$role->name}}</span>
                                    @endforeach
                                </td>
                                <td>@include("partials.is_active_icon", ["active" => $user->is_active])</td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pull-right">{{$users->appends(\Request::except('page'))->render()}}</div>
            @endcomponent
        </div>
    </div>
@stop
