@extends("layouts.robust.admin")

@section("content_header")
    @include("layouts.robust.partials.content_header", ["title" => "Create User"])
@stop

@section("content")
    <div class="row">
        <div class="col-xs-12">
            @component('admin.partials.card', ['cardTitle' => 'Create User', 'cardText' => 'Enter the details below to create a new user'])
                <create-edit-user
                        cancel-url="{{route('users.index')}}"
                        save-url="{{route('users.store')}}"></create-edit-user>
            @endcomponent
        </div>
    </div>
@stop

