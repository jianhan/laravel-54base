@extends("layouts.robust.admin")

@section("content")
    <section id="basic-form-layouts">
        @include('partials.form_validation_errors')
        @include('flash::message')
        {{Form::open(['route' => 'admin.settings.bulk_update', 'class' => 'form', 'method' => 'POST'])}}
        <div class="row match-height">
            <div class="col-md-6">
                <div class="card" style="height: auto;">
                    @include("admin.setting.partials.card_header", ["icon" => "icon-server2", "name" => "Genreal"])
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$business_name['key']}}">
                                                Business Name @include("partials.required")
                                            </label>
                                            {{Form::text($business_name['key'],
                                                        $business_name['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Business name which will be displayed in website and emails such as
                                                newsletter.
                                            @endcomponent
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$business_description['key']}}">
                                                Business Description @include("partials.required")
                                            </label>
                                            {{Form::textarea($business_description['key'],
                                                        $business_description['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                A short description of your business will be displayed in website and
                                                emails such as newsletter.
                                            @endcomponent
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="founded_date">
                                                Business Founded Date @include("partials.required")
                                            </label>
                                            {{Form::date($founded_date['key'],
                                                        $founded_date['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Date which business was founded, will be displayed on footer of you page
                                                as well as in all email sent out.
                                            @endcomponent
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="height: auto;">
                    @include("admin.setting.partials.card_header", ["icon" => "icon-android-share-alt", "name" => "Contact"])
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$contact_name['key']}}">
                                                Contact Person's Name
                                            </label>
                                            {{Form::text($contact_name['key'],
                                                        $contact_name['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Name of the person who will be contacted from your website, also will be
                                                displayed in outgoing emails.
                                            @endcomponent
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$contact_email['key']}}">
                                                Contact Email @include("partials.required")
                                            </label>
                                            {{Form::text($contact_email['key'],
                                                        $contact_email['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Email address for general contacting from public users to your website,
                                                also will be displayed in outgoing emails.
                                            @endcomponent
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$contact_phone['key']}}">
                                                Contact Phone Number
                                            </label>
                                            {{Form::text($contact_phone['key'],
                                                        $contact_phone['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Phone number genreal public will contact from, can be either mobile or
                                                local phone number.
                                            @endcomponent
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row match-height">
            <div class="col-md-6">
                <div class="card" style="height: auto;">
                    @include("admin.setting.partials.card_header", ["icon" => "icon-map6", "name" => "Address"])
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="address">
                                                Business Address
                                            </label>
                                            <setting-address address="{{$address['value']}}"
                                                             suburb="{{$suburb['value']}}"
                                                             state="{{$state['value']}}"
                                                             postcode="{{$postcode['value']}}"
                                                             country="{{$country['value']}}"
                                                             lat="{{$lat['value']}}"
                                                             lng="{{$lng['value']}}"
                                            ></setting-address>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="height: auto;">
                    @include("admin.setting.partials.card_header", ["icon" => "icon-android-share-alt", "name" => "Social Media"])
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$facebook['key']}}">
                                                Facebook
                                            </label>
                                            {{Form::text($facebook['key'],
                                                        $facebook['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Facebook page address will be displayed on your website as well as in
                                                all outgoing emails
                                            @endcomponent
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$twitter['key']}}">
                                                Twitter
                                            </label>
                                            {{Form::text($twitter['key'],
                                                        $twitter['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Twitter page address will be displayed on your website as well as in all
                                                outgoing emails
                                            @endcomponent
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$google_plus['key']}}">
                                                Google Plus
                                            </label>
                                            {{Form::text($google_plus['key'],
                                                        $google_plus['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Google plus page address will be displayed on your website as well as in
                                                all outgoing emails
                                            @endcomponent
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$instagram['key']}}">
                                                Instagram
                                            </label>
                                            {{Form::text($instagram['key'],
                                                        $instagram['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Instagram page address will be displayed on your website as well as in
                                                all outgoing emails
                                            @endcomponent
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$linkedin['key']}}">
                                                Linkedin
                                            </label>
                                            {{Form::text($linkedin['key'],
                                                        $linkedin['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Linkedin page address will be displayed on your website as well as in
                                                all outgoing emails
                                            @endcomponent
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="{{$youtube['key']}}">
                                                Youtube
                                            </label>
                                            {{Form::text($youtube['key'],
                                                        $youtube['value'],
                                                        ['class' => 'form-control'])}}
                                            @component('partials.help_text')
                                                Youtube channel address will be displayed on your website as well as in
                                                all outgoing emails
                                            @endcomponent
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions" style="text-align:right;">
            @component('partials.submit_btn')
            @endcomponent
            @component('partials.cancel_btn', ['href' => route("admin.dashboard")])
            @endcomponent
        </div>
        {{Form::close()}}
    </section>
@stop

@section("content_header")
    @include("layouts.robust.partials.content_header", ["title" => "Settings"])
@stop