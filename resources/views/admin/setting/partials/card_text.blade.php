@if(!empty(trim($text)))
    <div class="card-text">
        <p>{{ $text }}</p>
    </div>
@endif