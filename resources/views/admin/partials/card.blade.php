<div class="card">
    <div class="card-header">
        <h4 class="card-title">{{$cardTitle}}</h4>
    </div>
    <div class="card-body collapse in">
        <div class="card-block card-dashboard">
            @if (isset($cardText) && $cardText != "")
                <p class="card-text">{{$cardText}}</p>
            @endif
            {{$slot}}
        </div>
    </div>
</div>