@if (isset($itemsPerPage))
    <?php $display = str_plural($target, $itemsPerPage); ?>
    <button style="margin-top: 1rem; margin-bottom: 1rem;"
            type="button"
            class="btn btn-secondary btn-min-width dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false">{{$itemsPerPage.' '.$display}} / page
    </button>
    <div class="dropdown-menu">
        <a class="dropdown-item"
           href="{{URL::route('users.index', ['itemsPerPage' => '10', 'query' => Request::get('query')], true)}}">10 per
            page</a>
        <a class="dropdown-item"
           href="{{URL::route('users.index', ['itemsPerPage' => '20', 'query' => Request::get('query')], true)}}">20 per
            page</a>
        <a class="dropdown-item"
           href="{{URL::route('users.index', ['itemsPerPage' => '50', 'query' => Request::get('query')], true)}}">50 per
            page</a>
        <a class="dropdown-item"
           href="{{URL::route('users.index', ['itemsPerPage' => '100', 'query' => Request::get('query')], true)}}">100 per
            page</a>
    </div>
@endif