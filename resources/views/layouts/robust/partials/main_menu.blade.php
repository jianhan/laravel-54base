<!-- main menu-->
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <!-- main menu header-->
{{--<div class="main-menu-header">--}}
{{--<input type="text" placeholder="Search" class="menu-search form-control round"/>--}}
{{--</div>--}}
<!-- / main menu header-->
    <!-- main menu content-->
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class="nav-item {{ isActiveRoute('admin.dashboard') }}">
                <a href="{{route('admin.dashboard')}}">
                    <i class="icon-home3"></i>
                    <span data-i18n="nav.support_raise_support.main" class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item has-sub">
                <a href="#"><i class="icon-stack-2"></i><span data-i18n="nav.page_layouts.main"
                                                                                       class="menu-title">Page layouts</span></a>
                <ul class="menu-content">
                    <li class="is-shown"><a href="layout-1-column.html" data-i18n="nav.page_layouts.1_column"
                                            class="menu-item">1 column</a>
                    </li>
                    <li class="is-shown"><a href="layout-2-columns.html" data-i18n="nav.page_layouts.2_columns"
                                            class="menu-item">2 columns</a>
                    </li>
                    <li class="is-shown"><a href="layout-boxed.html" data-i18n="nav.page_layouts.boxed_layout"
                                            class="menu-item">Boxed layout</a>
                    </li>
                    <li class="is-shown"><a href="layout-static.html" data-i18n="nav.page_layouts.static_layout"
                                            class="menu-item">Static layout</a>
                    </li>
                    <li class="navigation-divider is-shown"></li>
                    <li class="is-shown"><a href="layout-light.html" data-i18n="nav.page_layouts.light_layout"
                                            class="menu-item">Light layout</a>
                    </li>
                    <li class="is-shown"><a href="layout-dark.html" data-i18n="nav.page_layouts.dark_layout"
                                            class="menu-item">Dark layout</a>
                    </li>
                    <li class="is-shown"><a href="layout-semi-dark.html" data-i18n="nav.page_layouts.semi_dark_layout"
                                            class="menu-item">Semi dark layout</a>
                    </li>
                </ul>
            </li>
            {{--<li class=" nav-item"><a href="index.html"><i class="icon-home3"></i><span data-i18n="nav.dash.main"--}}
            {{--class="menu-title">Dashboard</span><span--}}
            {{--class="tag tag tag-primary tag-pill float-xs-right mr-2">2</span></a>--}}
            {{--<ul class="menu-content">--}}
            {{--<li><a href="index.html" data-i18n="nav.dash.main" class="menu-item">Dashboard</a>--}}
            {{--</li>--}}
            {{--<li><a href="dashboard-2.html" data-i18n="nav.dash.main" class="menu-item">Dashboard 2</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class=" nav-item"><a href="#"><i class="icon-stack-2"></i><span data-i18n="nav.page_layouts.main"--}}
            {{--class="menu-title">Page layouts</span></a>--}}
            {{--<ul class="menu-content">--}}
            {{--<li><a href="layout-1-column.html" data-i18n="nav.page_layouts.1_column" class="menu-item">1--}}
            {{--column</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</li>--}}

            {{--<li class=" navigation-header"><span data-i18n="nav.category.support">Support</span><i data-toggle="tooltip"--}}
            {{--data-placement="right"--}}
            {{--data-original-title="Support"--}}
            {{--class="icon-ellipsis icon-ellipsis"></i>--}}
            {{--</li>--}}
            {{--<li class=" nav-item"><a href="https://github.com/pixinvent/robust-free-bootstrap-admin-template/issues"><i--}}
            {{--class="icon-support"></i><span data-i18n="nav.support_raise_support.main"--}}
            {{--class="menu-title">Raise Support</span></a>--}}
            {{--</li>--}}
            {{--<li class=" nav-item"><a href="https://pixinvent.com/free-bootstrap-template/robust-lite/documentation"><i--}}
            {{--class="icon-document-text"></i><span data-i18n="nav.support_documentation.main"--}}
            {{--class="menu-title">Documentation</span></a>--}}
            {{--</li>--}}
        </ul>
    </div>
    <!-- /main menu content-->
    <!-- main menu footer-->
    <!-- include includes/menu-footer-->
    <!-- main menu footer-->
</div>
<!-- / main menu-->