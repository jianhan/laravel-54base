@if($active === true)
    <span class="tag tag-success tag-pill float-xs-right"><i class="icon-check2"></i></span>
@else
    <span class="tag tag-danger tag-pill float-xs-right"><i class="icon-ban2"></i></span>
@endif
