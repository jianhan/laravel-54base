<div class="alert fade in mb-2 alert-{{$level}} {{isset($dismissable) && $dismissable === true ? "alert-dismissible" : ""}}"
     role="alert">
    @if(isset($dismissable) && $dismissable === true)
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    @endif
    {{ $slot }}
</div>