<?php $btnText = isset($text) ? $text : "save"; ?>
<button type="submit" class="btn btn-primary">
    <i class="icon-check2"></i> {{$btnText}}
</button>