<?php $btnText = isset($text) ? $text : "cancel"; ?>
<a href="{{$href}}" class="btn btn-warning mr-1">
    <i class="icon-cross2"></i> {{$btnText}}
</a>