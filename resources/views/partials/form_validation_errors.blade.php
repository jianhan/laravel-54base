@if ($errors->any())
    <div class="alert alert-danger mb-2" role="alert">
        <ul class="no-list-style mb-0 ml-0 pl-0">
            @foreach ($errors->all() as $error)
                <li>
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    </div>
@endif