<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use User;

class Address extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = [
        'lat' => 'double',
        'lng' => 'double'
    ];

    public function users()
    {
        return $this->morphedByMany(User::class, 'addressable');
    }
}