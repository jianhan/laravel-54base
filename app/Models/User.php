<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Hash;
use EloquentFilter\Filterable;
use App\ModelFilters\UserFilter;
use Kyslik\ColumnSortable\Sortable;
use App\Traits\ModelUtilities;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Addressable;

class User extends Authenticatable
{

    use Notifiable, HasRoles, Filterable, Sortable, ModelUtilities, Searchable, SoftDeletes, Addressable;

    protected $with = ['roles'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'provider_data' => 'array',
        'is_active' => 'boolean',
    ];

    public function modelFilter()
    {
        return $this->provideFilter(UserFilter::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * setPasswordAttribute mutator automatically hash password via eloquent model.
     *
     * @param $password
     *
     * @return void
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Get the index name for the model for laravel scout
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'users_index';
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return $this->toArray();
    }

    public function getHasAvatarUrlAttribute()
    {
        if (trim($this->avatar_url) != "") {
            return true;
        }
        return false;
    }


}
