<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Setting extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = [
        'visiable' => 'boolean'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('visible', function (Builder $builder) {
            $builder->where('visible', true);
        });
    }

}
