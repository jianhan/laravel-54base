<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddressType extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = [
        'displaying_order' => 'integer'
    ];

}