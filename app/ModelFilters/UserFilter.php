<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

/**
 * Class UserFilter define filters for user model.
 *
 * @package App\ModelFilters
 */
class UserFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relatedModel => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    /**
     * Handle filter for query string
     *
     * @param string $queryString
     *
     * @return $this
     */
    public function query(string $queryString)
    {
        if(empty(trim($queryString))) {
            return $this;
        }

        return $this->where(function ($q) use ($queryString) {
            $q->where('name', 'like', '%'.$queryString.'%')->orWhere('email', 'like', '%'.$queryString.'%');
        });

    }

}
