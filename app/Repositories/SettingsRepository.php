<?php namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use App\Models\Setting;

class SettingsRepository extends Repository
{

    public function model()
    {
        return Setting::class;
    }

    public function bulkUpdate(array $array)
    {
        if (empty($array)) {
            throw new \InvalidArgumentException("Settings are empty");
        }

        foreach ($array as $k => $v) {
            Setting::where('key' , $k)->update(["value" => $v]);
        }
    }
}