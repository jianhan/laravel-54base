<?php

namespace App\Exceptions;

use Exception;

class ViewNotFoundException extends Exception
{
    public static function fileMissing(string $path)
    {
        return new static("The view trying to load does not exists : $path");
    }
}
