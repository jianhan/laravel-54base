<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Admin dashboard controller
 */
class DashboardController extends Controller
{

    /**
     * Index action renders view for dashboard index page.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return $this->autoRender();
    }

    /**
     * redirectToIndex redirects /admin to /admin/dashboard.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToIndex(Request $request)
    {
        return redirect(route("admin.dashboard"));
    }
}
