<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Permission;

class RolesPermissionsManagementController extends Controller
{
    public function index(Request $request)
    {
        $roles = Role::with('perms')->orderBy("display_name")->get();
        $permissions = Permission::orderBy('display_name')->get();
        return $this->autoRender(get_defined_vars());
    }
}
