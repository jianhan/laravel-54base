<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Console\DetectsApplicationNamespace;
use App\Exceptions\ViewNotFoundException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, DetectsApplicationNamespace;

    protected function autoRender(array $data = [], bool $viewName = false)
    {
        $reflect = new \ReflectionClass(static::class);
        $namespace = trim(str_replace($this->getAppNamespace() . 'Http\Controllers', '', $reflect->getNamespaceName()),
            '\\');
        if (!ends_with($reflect->getShortName(), 'Controller')) {
            throw AutoRenderInvalidControllerName::invalidControllerName($reflect->getShortName());
        }
        $namespaceFolderName = empty($namespace) ? null : str_slug($namespace) . DIRECTORY_SEPARATOR;
        $namespaceFolder = empty($namespace) ? null : str_slug($namespace) . '.';
        $viewFolderName = snake_case(str_replace('Controller', '', $reflect->getShortName()));
        $viewName = $viewName ?: debug_backtrace()[1]['function'];
        $viewPath = resource_path('views' . DIRECTORY_SEPARATOR . $namespaceFolderName . $viewFolderName . DIRECTORY_SEPARATOR . $viewName . '.blade.php');
        if (!file_exists($viewPath)) {
            throw ViewNotFoundException::fileMissing($viewPath);
        }
        return view($namespaceFolder . $viewFolderName . '.' . $viewName, $data);
    }
}
