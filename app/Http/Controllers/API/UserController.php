<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Repositories\UsersRepository;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Traits\APIPagination;

class UserController extends Controller
{
    use APIPagination;

    private $usersRepository;

    /**
     * UserController constructor.
     *
     * @param $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    public function dataSource(Request $request)
    {
        extract($this->preparePagination($request), EXTR_SKIP);
        return User::filter($request->all())->paginate($limit)->toJson();
    }
}
