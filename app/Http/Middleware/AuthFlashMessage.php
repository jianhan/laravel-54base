<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate;
use Closure;

class AuthFlashMessage extends Authenticate
{
    /**
     * Handle an incoming request, will add flash message by extending parent
     * class.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        try {
            return parent::handle($request, $next, $guards);
        } catch (\Exception $e) {
            flash("Please login to proceed", "warning");
            throw $e;
        }
    }
}
