<?php

namespace App\Traits;
use Carbon\Carbon;

trait ModelUtilities
{
    /**
     * Format date will be used only for eloquent model to be able to handle invalid date
     * from mysql or any invalid date string.
     *
     * @param  string Attribute in eloquent model.
     * @param  string Output format of the date for the attribute in eloquent model.
     *
     * @return string|boolean If the date is valid, then return the corresponding date format, otherwise return false.
     */
    public function formatDate($field, $format = 'd-m-Y')
    {
        if (!isset($this->{$field})) {
            return null;
        }
        $field = $this->{$field} instanceof Carbon ? $this->{$field}->__toString() : $this->{$field};
        if (starts_with($field, '0000') || starts_with($field, '-')) {
            return null;
        }
        $isTimestamp = strpos($field, ' ') !== false;
        return Carbon::createFromFormat('Y-m-d' . ($isTimestamp ? ' H:i:s' : ''), $field)->format($format);
    }
}
