<?php

namespace App\Traits;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

trait APIPagination
{
    protected function preparePagination(Request $request) : array
    {
        $limit = $request->get("limit", 10);
        $offset = $request->get("offset", 0);
        $currentPage = ($offset / $limit) + 1;
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

        return get_defined_vars();
    }
}