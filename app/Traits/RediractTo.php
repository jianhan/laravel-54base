<?php

namespace App\Traits;

trait RediractTo
{
    protected function redirectTo()
    {
        return \Config::get('auth.redirect_after_login');
    }
}
