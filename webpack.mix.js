let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    module: {
        loaders: [{
            test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
            loader: 'file-loader'
        },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]

    }
})

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/plugins.js', 'public/js')
    .scripts([
        'resources/robust/app-assets/vendors/js/ui/tether.min.js',
        'resources/robust/app-assets/js/core/libraries/bootstrap.min.js',
        'resources/robust/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js',
        'resources/robust/app-assets/vendors/js/ui/unison.min.js',
        'resources/robust/app-assets/vendors/js/ui/blockUI.min.js',
        'resources/robust/app-assets/vendors/js/ui/jquery.matchHeight-min.js',
        'resources/robust/app-assets/vendors/js/ui/screenfull.min.js',
        'resources/robust/app-assets/vendors/js/extensions/pace.min.js',
        'resources/robust/app-assets/js/core/app-menu.js',
        'resources/robust/app-assets/js/core/app.js',
    ], 'public/js/robust.js')
    .styles([
        'resources/robust/app-assets/css/bootstrap.css',
        'resources/robust/app-assets/fonts/icomoon.css',
        'resources/robust/app-assets/fonts/flag-icon-css/css/flag-icon.min.css',
        'resources/robust/app-assets/vendors/css/extensions/pace.css',
        'resources/robust/app-assets/css/bootstrap-extended.css',
        'resources/robust/app-assets/css/app.css',
        'resources/robust/app-assets/css/colors.css',
        'resources/robust/app-assets/css/core/menu/menu-types/vertical-menu.css',
        'resources/robust/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css',
        'resources/robust/app-assets/css/pages/login-register.css',
        'resources/robust/assets/css/style.css',
    ], 'public/css/robust.css')
    .mix.copy('resources/robust/app-assets/images', 'public/app-assets/images')
    .mix.copy('resources/robust/app-assets/fonts/icomoon', 'public/css/icomoon');

