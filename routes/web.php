<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

// administrators section of routes
Route::group(['middleware' => ['auth_flash_message'], 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('roles-permissions-management', 'RolesPermissionsManagementController@index')->name('admin.roles-permissions-management');
    Route::resource('settings', 'SettingController');
    Route::resource('users', 'UserController');
    Route::post('settings/bulk-update', 'SettingController@bulkUpdate')->name('admin.settings.bulk_update');
});

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->name('socilite_login');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('socilite_callback');
