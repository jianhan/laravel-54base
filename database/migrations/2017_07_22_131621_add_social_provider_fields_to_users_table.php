<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialProviderFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('email')->nullable()->change();
            $table->string('password')->nullable()->change();
            $table->string('provider')->nullable()->default(null);
            $table->string('provider_id')->nullable()->default(null);
            $table->text('provider_data')->nullable()->default(null);
            $table->string('avatar_url')->nullable()->default(null);
            // drop unique for email, because if a user register a facebook and twitter
            // with the same email address and tried to login, then it will cause issue
            $table->dropUnique(['email']);
            $table->unique(['email', 'provider_id'], 'unique_email_with_provider');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['provider', 'provider_id']);
        });
    }
}
