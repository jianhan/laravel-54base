<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('formatted_address');
            $table->string('street_number')->nullable()->default(null);
            $table->string('route')->nullable()->default(null);
            $table->string('administrative_area_level_2')->nullable()->default(null);
            $table->string('administrative_area_level_1');
            $table->string('country');
            $table->double('lat', 10, 8);
            $table->double('lng', 10, 8);
            $table->string('postal_code');
            $table->text('data');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('address_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('displaying_order')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('addressables', function (Blueprint $table) {
            $table->integer('address_id')->unsigned();
            $table->integer('address_type_id')->unsigned();
            $table->morphs('addressable');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('address_id')
                ->references('id')
                ->on('addresses')
                ->onDelete('CASCADE');
            $table->foreign('address_type_id')
                ->references('id')
                ->on('address_types')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('address_types');
        Schema::dropIfExists('addressable');
        Schema::enableForeignKeyConstraints();
    }
}
