<?php

use Illuminate\Database\Seeder;
use App\Models\AddressType;

class AddressTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeding data array
        $data = [
            [
                'name' => 'Shipping Address',
                'description' => 'Shipping address',
                'displaying_order' => 1,
            ],
            [
                'name' => 'Billing Address',
                'description' => 'Billing address',
                'displaying_order' => 2,
            ],
            [
                'name' => 'Residential Address',
                'description' => 'Residential address',
                'displaying_order' => 3,
            ],
            [
                'name' => 'Postal Address',
                'description' => 'Postal address',
                'displaying_order' => 4,
            ],
        ];

        foreach ($data as $k => $v) {
            $addressType = AddressType::create($v);
        }
    }
}
