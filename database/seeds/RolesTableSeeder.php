<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeding data array
        $data = [
            [
                'name' => 'owner',
                'description' => 'Role represent the owner of the project',
                'all_permissions' => true,
            ],
            [
                'name' => 'super user',
                'description' => 'Role represent super user',
                'all_permissions' => true,
            ],
            [
                'name' => 'administrator',
                'description' => 'Role represent administrator',
            ],
            [
                'name' => 'site manager',
                'description' => 'Role represent site manager'
            ],
            [
                'name' => 'customer',
                'description' => 'Role represent customer'
            ],
        ];

        foreach ($data as $k => $v) {
            $allPermissions = isset($v['all_permissions']) ? $v['all_permissions'] : false;
            unset($v['all_permissions']);
            $role = Role::create($v);
            if ($allPermissions === true) {
                $role->givePermissionTo(Permission::all());
            }
        }
    }
}
