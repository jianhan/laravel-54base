<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Users table seeder to initialize default users
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeding data array
        $data = [
            [
                'name' => 'Owner',
                'email' => 'owner@email.com',
                'password' => 'password',
                'remember_token' => str_random(10),
                'roles' => [
                    'owner',
                    'super-user',
                    'administrator',
                    'site-manager',
                ]
            ],
            [
                'name' => 'Super User',
                'email' => 'super-user@email.com',
                'password' => 'password',
                'remember_token' => str_random(10),
                'roles' => [
                    'super-user',
                    'administrator',
                    'site-manager',
                ]
            ],
            [
                'name' => 'Administrator',
                'email' => 'administrator@email.com',
                'password' => 'password',
                'remember_token' => str_random(10),
                'roles' => [
                    'administrator',
                    'site-manager',
                ]
            ],
            [
                'name' => 'Site Manager',
                'email' => 'site-manager@email.com',
                'password' => 'password',
                'remember_token' => str_random(10),
                'roles' => [
                    'site-manager',
                ]
            ],
            [
                'name' => 'Customer A',
                'email' => 'customer@email.com',
                'password' => 'password',
                'remember_token' => str_random(10),
                'roles' => [
                    'customer',
                ]
            ],
        ];

        foreach ($data as $k => $v) {
            $roles = $v['roles'];
            unset($v['roles']);
            $user = User::create($v);
            foreach ($roles as $name) {
                if ($role = Role::where('name', $name)->first()) {
                    $user->assignRole($role);
                }
            }
        }
    }
}
