<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'key' => 'business_name',
                'value' => 'My Business',
            ],
            [
                'key' => 'business_description',
                'value' => 'My Business Description',
            ],
            [
                'key' => 'founded_date',
                'value' => '01-01-2017',
            ],
            [
                'key' => 'contact_name',
                'value' => 'Jim Smith',
            ],
            [
                'key' => 'contact_email',
                'value' => 'info@gmail.com',
            ],
            [
                'key' => 'contact_phone',
                'value' => '02342332434',
            ],
            [
                'key' => 'facebook',
                'value' => 'http://facebook.com',
            ],
            [
                'key' => 'twitter',
                'value' => 'http://twitter.com',
            ],
            [
                'key' => 'google_plus',
                'value' => 'http://google.com',
            ],
            [
                'key' => 'instagram',
                'value' => 'http://instagram.com',
            ],
            [
                'key' => 'linkedin',
                'value' => 'http://linkedin.com',
            ],
            [
                'key' => 'youtube',
                'value' => 'http://youtube.com',
            ],
            [
                'key' => 'address',
                'value' => 'test address',
            ],
            [
                'key' => 'suburb',
                'value' => '',
            ],
            [
                'key' => 'state',
                'value' => '',
            ],
            [
                'key' => 'postcode',
                'value' => '',
            ],
            [
                'key' => 'country',
                'value' => '',
            ],
            [
                'key' => 'lat',
                'value' => '',
            ],
            [
                'key' => 'lng',
                'value' => '',
            ],
            [
                'key' => 'postal_address',
                'value' => 'abc PO box',
            ],
        ];


        foreach ($data as $d) {
            if (!isset($d['name'])) {
                $d['name'] = titleize($d['key']);
            }
            if (!isset($d['description'])) {
                $d['description'] = titleize($d['key']);
            }
            Setting::create($d);
        }
    }
}
