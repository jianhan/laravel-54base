<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeding data array
        $data = [
            // users related permission
            [
                'name' => 'Manage Users',
                'description' => 'Permission to manage users'
            ],
            [
                'name' => 'View Users',
                'description' => 'Permission to view users'
            ],
            [
                'name' => 'Create User',
                'description' => 'Permission to create user'
            ],
            [
                'name' => 'Edit User',
                'description' => 'Permission to edit user'
            ],
            [
                'name' => 'Delete User',
                'description' => 'Permission to delete user'
            ],
            // permissions to manage roles & permissions
            [
                'name' => 'Manage Roles Permissions',
                'description' => 'Permission manage roles and permissions'
            ],
        ];

        foreach ($data as $k => $v) {
            $owner = new Permission();
            $owner->create($v);
        }
    }
}
